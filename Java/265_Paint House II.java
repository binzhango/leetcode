/*
There are a row of n houses, each house can be painted with one of the k colors. 
The cost of painting each house with a certain color is different. 
You have to paint all the houses such that no two adjacent houses have the same color.
The cost of painting each house with a certain color is represented by a n x k cost matrix.
For example, costs0 is the cost of painting house 0 with color 0; 
costs1 is the cost of painting house 1 with color 2, and so on... 
Find the minimum cost to paint all houses.
Note: All costs are positive integers.
Follow up: Could you solve it in O(nk) runtime?
*/

public class Solution{
    public int minCostII(int [][] costs){
        if(costs==null || costs.length==0) return 0;
        int m = costs.length, n = costs[0].length; //size of the matrix
        int preMin = 0, preIdx =-1, preSec =0;
        for(int i=0 ;i<m;i++){
            int curMin = Integer.MAX_VALUE, curIdx =-1, curSec = Integer.MAX_VALUE;
            for(int j =0 ;j<n;j++){
                int val = cost[i][j]+(preIdx==j ? preSec:preMin);
                if(val<curMin){
                    curSec = curMin;
                    curMin = val;
                    curIdx =j;
                }else if(val<curSec){
                    curSec = val;
                }
            }
            preMin = curMin;
            preSec = curSec;
            preIdx = curIdx;
        }
        return preMin;
    }
}