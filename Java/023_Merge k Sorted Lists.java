/*Merge k sorted linked lists and return it as one sorted list.
 Analyze and describe its complexity. 
 */

 /**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */

 public class Solution {
    // public ListNode mergeKLists(ListNode[] lists) {
    //     if(lists == null || lists.length ==0) return null;
    //     return helper(lists,0,lists.length-1);
    // }
    // private ListNode helper(ListNode[] lists, int l, int h){
    //     if(l<h){
    //         int m = (l+h)/2;
    //         return merge(helper(lists,l,m),helper(lists,m+1,h));
    //     }
    //     return lists[l];
    // }
    // private ListNode merge(ListNode l1, ListNode l2){
    //     ListNode result = new ListNode(0);
    //     ListNode head = result;
        
    //     while(l1!=null && l2!=null){
    //         if(l1.val<l2.val){
    //             head.next =l1;
    //             l1 = l1.next;
    //         }else{
    //             head.next =l2;
    //             l2 = l2.next;
    //         }
    //         head = head.next;
    //     }
    //     while(l1!=null) head.next =l1;
    //     while(l2!=null) head.next =l2;
    //     return result.next;
        
    // }
    public ListNode mergeKLists(ListNode[] lists) {
        if(lists ==null || lists.length ==0) return null;
        // PriorityQueue <ListNode> pq = new PriorityQueue<>(new Comparator<ListNode>(){
        //     @Override
        //     public int compare(ListNode l1, ListNode l2){
        //         return l1.val-l2.val;
        //     }
        // });
        Comparator<ListNode> compare = (l1, l2) -> l1.val-l2.val;
        PriorityQueue<ListNode> pq = new PriorityQueue<>(compare);
        ListNode result = new ListNode(0);
        ListNode head = result;
        
        for(ListNode l:lists){
            if(l!=null)
            pq.offer(l);
        }
        
        while(!pq.isEmpty()){
            ListNode n = pq.poll();
            head.next = n;
            head = head.next;
            
            if(n.next!=null){
                pq.offer(n.next);
            }
        }
        return result.next;
    }
}
