/*
Given a set of distinct integers, nums, return all possible subsets.
Note: The solution set must not contain duplicate subsets.
For example,
If nums = [1,2,3], a solution is:
[
  [3],
  [1],
  [2],
  [1,2,3],
  [1,3],
  [2,3],
  [1,2],
  []
] 
*/

/*
Pick a starting point.
while(Problem is not solved)
    For each path from the starting point.
        check if selected path is safe, if yes select it
        and make recursive call to rest of the problem
        before which undo the current move.
    End For
If none of the move works out, return false, NO SOLUTON.
*/

public class Solution {
    public List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(result, new ArrayList<>(), nums,0);
        return result;
    }
    
    public void backtrack(List<List<Integer>> result, List<Integer> templist, int[] nums, int start){
        result.add(new ArrayList<>(templist)); //copy the templist to result
        for(int i=start;i<nums.length;i++){
            templist.add(nums[i]);
            backtrack(result, templist, nums, i+1);
            templist.remove(templist.size()-1);
        }
    }
}