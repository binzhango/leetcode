/*
Given a binary tree and a sum, determine if the tree has a root-to-leaf path 
such that adding up all the values along the path equals the given sum.
For example:
Given the below binary tree and sum = 22,

              5
             / \
            4   8
           /   / \
          11  13  4
         /  \      \
        7    2      1

return true, as there exist a root-to-leaf path 5->4->11->2 which sum is 22.
*/


/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */

//recursive
public class Solution {
    public boolean hasPathSum(TreeNode root, int sum) {
        if(root == null) return false;
        if(root.left == null && root.right == null) return root.val==sum;
        return hasPathSum(root.left, sum-root.val) || hasPathSum(root.right, sum-root.val);
    }
}

//DFS
public class Solution{
    public boolean hasPathSum(TreeNode root, int sum){
        Deque<TreeNode> stack1 = new ArrayDeque<>();
        Deque<Integer> stack2 = new ArrayDeque<>();
        if(root == null) return false;
        stack1.push(root);
        stack2.push(sum);
        
        while(!stack1.isEmpty()){
            TreeNode node = stack1.pop();
            int value = stack2.pop();
            
            if(node.left == null && node.right==null && node.val==value){
                return true;
            }
            if(node.left!=null){
                stack1.push(node.left);
                stack2.push(value-node.val);
            }
            if(node.right!=null){
                stack1.push(node.right);
                stack2.push(value-node.val);
            }


        }
        return false;
    }
}



    public boolean hasPathSum(TreeNode root, int sum){
        Deque<TreeNode> stack1 = new ArrayDeque<>();
        Deque<Integer> stack2 = new ArrayDeque<>();
        if(root == null) return false;
        
        stack1.push(root);
        stack2.push(root.val);
        
        while(!stack1.isEmpty()){
            TreeNode node = stack1.pop();
            int value = stack2.pop();
            
            if(node.left == null && node.right==null && value == sum){
                return true;
            }
            if(node.right!=null){
                stack1.push(node.right);
                stack2.push(value+node.right.val);
            }
            
            if(node.left!=null){
                stack1.push(node.left);
                stack2.push(value+node.left.val);
            }

        }  
        return false;
    }
}





    