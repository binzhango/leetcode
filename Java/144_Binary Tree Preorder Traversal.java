/*
Given a binary tree, return the preorder traversal of its nodes' values.

For example:
Given binary tree {1,#,2,3},

   1
    \
     2
    /
   3

return [1,2,3]. 
*/

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public List<Integer> preorderTraversal(TreeNode root) {
        Deque<TreeNode> stack = new ArrayDeque<>();
        TreeNode p = root;
        List<Integer> list = new ArrayList<>();
        while(p!=null || !stack.isEmpty()){
            if(p!=null){
                stack.push(p);
                list.add(p.val);
                p = p.left;
            }else{
                TreeNode node = stack.pop();
                p=node.right;
            }
        }
        return list;
    }
}