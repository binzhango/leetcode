/*Merge two sorted linked lists and return it as a new list. 
The new list should be made by splicing together the nodes of the first two lists.
*/

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */

 public class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if(l1 == null) return l2;
        if(l2 == null) return l1;
        
        ListNode head = new ListNode(0);
        ListNode cur = head;
        
        while(l1 !=null && l2 != null){
            if(l1.val>=l2.val){
                cur.next =l2;
                l2 = l2.next;
            }else{
                cur.next = l1;
                l1 = l1.next;
            }
            cur = cur.next;
        }
        
        if(l1!=null) cur.next =l1;
        if(l2!=null) cur.next =l2;
        return head.next;

    }
}





/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
class Solution {
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if(l1==null) return l2;
        if(l2==null) return l1;
        
        ListNode l = new ListNode(0);
        ListNode temp =l;
        
        if(l1.val<l2.val){
            temp.next = l1;
            temp = temp.next; //update temp
            temp.next = mergeTwoLists(l1.next,l2);
        }else{
            temp.next =l2;
            temp = temp.next;
            temp.next = mergeTwoLists(l1,l2.next);
            
        }
        return l.next;
    }
}
