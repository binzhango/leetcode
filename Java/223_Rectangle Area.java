/*
Find the total area covered by two rectilinear rectangles in a 2D plane.
Each rectangle is defined by its bottom left corner and top right corner 
as shown in the figure.

*/


public class Solution {
    public int computeArea(int A, int B, int C, int D, int E, int F, int G, int H) {
        
        int s1 = (C-A)*(D-B);
        int s2 =(G-E)*(H-F);
        
        int left = A>=E? A:E;
        int right = C>=G? G:C;
        int top = D>=H? H:D;
        int bottom = B>=F? B:F;
        
        int share =0;
        if(right>left && top>bottom) share = (right-left)*(top-bottom);
        return s1+s2-share;
    }
}