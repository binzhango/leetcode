/*
Given a Binary Search Tree and a target number, return true if 
there exist two elements in the BST such that their sum is equal 
to the given target.

Example 1:

Input: 
    5
   / \
  3   6
 / \   \
2   4   7

Target = 9

Output: True

Example 2:

Input: 
    5
   / \
  3   6
 / \   \
2   4   7

Target = 28

Output: False


*/



/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
class Solution {
    public boolean findTarget(TreeNode root, int k) {
        
        Set<Integer> set = new HashSet<>();
        return dfs(root, set, k);
        
    }
    public boolean dfs(TreeNode root, Set<Integer> set, int k){
        if(root==null) return false;
        if(set.contains(k-root.val)) return true;
        set.add(root.val);
        return dfs(root.left, set, k) || dfs(root.right, set, k);
    }
}


//convert the BST to an sorted array by inorder traversal
class Solution {
    public boolean findTarget(TreeNode root, int k) {
        List<Integer> list = new ArrayList<>();
        inorder(root, list);
        for(int i=0, j=list.size()-1;i<j;){
            if(list.get(i)+list.get(j)== k) return true;
            else if(list.get(i)+list.get(j)< k) i++;
            else j--;
        }
        
        return false;      
    }
    
    public void inorder(TreeNode root, List<Integer> list){
        if(root==null) return;
        inorder(root.left, list);
        list.add(root.val);
        inorder(root.right, list);
    }






//O(nlgn)
class Solution {
    public boolean findTarget(TreeNode root, int k) {
        return dfs(root, root, k);   
    }
    
    public boolean dfs(TreeNode root, TreeNode cur, int k){
        if(cur == null) return false;
        return search(root, cur, k-cur.val) || dfs(root, cur.left, k) || dfs(root, cur.right,k);
    }
    
    public boolean search(TreeNode root, TreeNode cur, int value){
        if(cur == null) return false;
        return (root.val==value && root != cur) || (root.val < value) && search(root.right, cur, value)
            || (root.val > value) && search(root.left, cur, value);
    }
    
}