/*
Say you have an array for which the ith element is the price of a given stock on day i.
If you were only permitted to complete at most one transaction (ie, buy one and sell one share of the stock), design an algorithm to find the maximum profit.

Example 1:
Input: [7, 1, 5, 3, 6, 4]
Output: 5

max. difference = 6-1 = 5 (not 7-1 = 6, as selling price needs to be larger than buying price)

Example 2:

Input: [7, 6, 4, 3, 1]
Output: 0

In this case, no transaction is done, i.e. max profit = 0.

*/

public class Solution{
    public int maxProfit(int [] prices){
        if(prices.length<2) return 0;
        int [] dp = new int[prices.length]; //dp[i] means that the max_profit ended at index i
                                            // thus the max_profit should be the last one    
        int minPrice = prices[0];
        dp[0] = 0;
        for(int i=1;i<prices.length;i++){
            if(prices[i]>=minPrice){
                dp[i]=Math.max(dp[i-1], prices[i]-minPrice);
            }else{
                minPrice = prices[i];
                dp[i] = dp[i-1];
            }
        }
        return dp[prices.length-1];
    }
}






public class Solution {
    public int maxProfit(int[] prices) {
        int min =0,max=0;
        if(prices==null||prices.length==0) return 0;
        min = prices[0];
        for(int i=1;i<prices.length;i++){
            min = Math.min(min, prices[i]); //each time should find the min price to buy
            max = Math.max(max,prices[i]-min); // based on the min then sell it
        }
        return max;
    }
}


//lambda expression
public class Solution {
    int min = Integer.MAX_VALUE;
    public int maxProfit(int[] prices) {
        return Arrays.stream(prices).map(i->i-(min = Math.min(min, i))).max().orElse(0);
    }
}