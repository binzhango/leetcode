/*

Given a 2D board and a word, find if the word exists in the grid.

The word can be constructed from letters of sequentially adjacent cell, 
where "adjacent" cells are those horizontally or vertically neighboring. 
The same letter cell may not be used more than once.

For example,
Given board =

[
  ['A','B','C','E'],
  ['S','F','C','S'],
  ['A','D','E','E']
]
word = "ABCCED", -> returns true,
word = "SEE", -> returns true,
word = "ABCB", -> returns false.
*/


//backtracking & DFS

class Solution {
    public boolean exist(char[][] board, String word) {
        int m = board.length;
        int n = board[0].length;
        boolean[][] visited = new boolean[m][n];
        
        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                if(dfs(board, word, visited, i,j,0)){
                    return true;
                }
            }
        }
        return false;
    }
    
    
    public boolean dfs(char[][] board, String word, boolean[][] visited, int i, int j, int index){
        if(word.length()==index) return true;
        if(i<0 || j<0 || i>=board.length || j>=board[0].length) return false;
        
        if(!visited[i][j] && word.charAt(index)==board[i][j]){
            visited[i][j] = true; //entry-backtrack
            boolean res= dfs(board, word, visited, i+1, j, index+1) || dfs(board, word, visited, i-1,j, index+1)
                    || dfs(board, word, visited, i, j+1, index+1) || dfs(board, word, visited, i,j-1, index+1);
            visited[i][j] = false; //exit-backtrack
            return res;
        }
        return false;
        
    }
    
}