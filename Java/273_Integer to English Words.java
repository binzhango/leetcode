/*
Convert a non-negative integer to its english words representation. Given input is guaranteed to be less than 231 - 1. 
For example,

123 -> "One Hundred Twenty Three"
12345 -> "Twelve Thousand Three Hundred Forty Five"
1234567 -> "One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven"
*/


/**
use recursion
*/
public class Solution {
    
    public static final String [] bellowTen ={"","One","Two", "Three", "Four", "Five", "Six", "Seven", "Eight","Nine"};
    public static final String [] bellowTwenty ={"Ten","Eleven","Twelve","Thirteen","Fourteen", "Fifteen","Sixteen","Seventeen","Eighteen","Nineteen"};
    public static final String [] bellowOneHundred ={"","Ten","Twenty", "Thirty", "Forty","Fifty","Sixty","Seventy","Eighty","Ninety"};
    public String numberToWords(int num) {
        if(num==0) return "Zero";
        return convert(num);
        
    }
    
    public String convert(int n){
        String result = new String();
        
        if(n<10) result = bellowTen[n];
        else if(n<20) result = bellowTwenty[n-10];
        else if(n<100) result = bellowOneHundred[n/10]+" "+convert(n%10);
        else if(n<1_000) result = convert(n/100)+" Hundred "+convert(n%100);
        else if(n<1_000_000) result = convert(n/1_000)+" Thousand "+convert(n%1_000);
        else if(n<1_000_000_000) result = convert(n/1_000_000)+" Million "+convert(n%1_000_000);
        else result = convert(n/1_000_000_000)+" Billion "+convert(n%1_000_000_000);
        return result.trim();
    }
}