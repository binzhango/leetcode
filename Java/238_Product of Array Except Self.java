/*
 Given an array of n integers where n > 1, nums, return an array output 
 such that output[i] is equal to the product of all the elements of nums 
 except nums[i].

Solve it without division and in O(n).

For example, given [1,2,3,4], return [24,12,8,6].

Follow up:
Could you solve it with constant space complexity? (Note: The output 
array does not count as extra space for the purpose of space complexity analysis.)

*/

class Solution {
    public int[] productExceptSelf(int[] nums) {
        int len = nums.length;
        int[] fwd = new int[len];
        int[] bwd = new int[len];
        int[] result = new int[len];
        fwd[0] =1;
        bwd[len-1] = 1;
        for(int i =1; i<len;i++){
            fwd[i] = fwd[i-1]*nums[i-1];
        }
        for(int i=len-2; i>=0;i--){
            bwd[i] = bwd[i+1]*nums[i+1];
        }
        
        for(int i =0; i<len;i++){
            result[i] = fwd[i]*bwd[i];
        }
        
        return result;
    }
}


//optimise the space
class Solution {
    public int[] productExceptSelf(int[] nums) {
        int len = nums.length;
        int[] result = new int[len];
        int right =1;
        result[0]=1;
        for(int i=1; i<len;i++){
            result[i] = result[i-1]*nums[i-1];
        }
        for(int i=len-1;i>=0;i--){
            result[i]*=right;
            right*=nums[i];
        }
        return result;
    }
}