/*
Given an unsorted array of integers, 
find the length of the longest consecutive elements sequence.
For example,
Given [100, 4, 200, 1, 3, 2],
The longest consecutive elements sequence is [1, 2, 3, 4]. Return its length: 4.
Your algorithm should run in O(n) complexity. 
*/


/*
 left            right
------l  n  r----------------------

n-left          sum                  n+right
   -----------------------------------

*/


public class Solution {
    public int longestConsecutive(int[] nums) {
        
        Map<Integer, Integer> map = new HashMap<Integer,Integer>();
        int max =0;
        for(int n :nums){
            if(map.containsKey(n)) continue;
            int left = map.containsKey(n-1)? map.get(n-1):0;
            int right = map.containsKey(n+1)? map.get(n+1):0;
            int sum = left+right+1;
            map.put(n,sum);
            
            //update the range of the length
            map.put(n-left,sum);
            map.put(n+right,sum);
            
            max=Math.max(max,sum);
        }
        return max;
        
    }
}