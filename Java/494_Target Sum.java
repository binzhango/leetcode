/*
 You are given a list of non-negative integers, a1, a2, ..., an, and a target, S.
Now you have 2 symbols + and -. For each integer, you should choose one from + and - as its new symbol.
Find out how many ways to assign symbols to make sum of integers equal to target S.

Example 1:

Input: nums is [1, 1, 1, 1, 1], S is 3. 
Output: 5
Explanation: 

-1+1+1+1+1 = 3
+1-1+1+1+1 = 3
+1+1-1+1+1 = 3
+1+1+1-1+1 = 3
+1+1+1+1-1 = 3

There are 5 ways to assign symbols to make the sum of nums be target 3.

sum(p)-sum(n) = target
sum(p)+sum(p)-sum(p)-sum(n) = target
2sum(p)-(sum(p)+sum(n)) = target
2sum(p)-sum(nums)=target
sum(p)=(target+sum(nums))/2;

each elment should be either in p or int n;

use dynamic programming




*/



public class Solution {
    public int findTargetSumWays(int[] nums, int S) {
        int sum =0;
        for(int i:nums){
            sum+=i;
        }
        if(sum<S || (sum+S)%2>0){
            return 0;
        }else{
           return subsetSum(nums, (sum+S)/2);
        }
        
    }
    
    public int subsetSum(int[] nums, int s){
        int[] dp = new int[s+1];
        dp[0]=1;
        for(int n:nums){
            for(int i=s;i>=n;i--){
                dp[i]+=dp[i-n];
            }
        }
        return dp[s];
    }
    
}