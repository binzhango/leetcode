/*
Given a collection of integers that might contain duplicates, nums, return all possible subsets.

Note: The solution set must not contain duplicate subsets.

For example,
If nums = [1,2,2], a solution is:

[
  [2],
  [1],
  [1,2,2],
  [2,2],
  [1,2],
  []
]

*/

public class Solution {
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(result, new ArrayList<>(), nums, 0);
        return result;
    }
    
    public void backtrack(List<List<Integer>> result, List<Integer> templist, int[] nums, int start){
        result.add(new ArrayList<>(templist));
        for(int i=start;i<nums.length;i++){
            if(i>start && nums[i]==nums[i-1]) continue; // only add one state to check the duplication
            templist.add(nums[i]);
            backtrack(result, templist, nums, i+1);
            templist.remove(templist.size()-1);
        }
    }
}