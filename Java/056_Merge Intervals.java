/*
Given a collection of intervals, merge all overlapping intervals.

For example,
Given [1,3],[2,6],[8,10],[15,18],
return [1,6],[8,10],[15,18].


*/


/**
 * Definition for an interval.
 * public class Interval {
 *     int start;
 *     int end;
 *     Interval() { start = 0; end = 0; }
 *     Interval(int s, int e) { start = s; end = e; }
 * }
 */


 //only sort the start
class Solution {
    public List<Interval> merge(List<Interval> intervals) {
        if(intervals.size() <=1) return intervals;
        intervals.sort((i1,i2) -> i1.start-i2.start);
        
        List<Interval> result = new LinkedList<>();
        
        int start = intervals.get(0).start;
        int end = intervals.get(0).end;
        
        for(Interval interval: intervals){
            if(interval.start<=end){
                end = Math.max(end, interval.end); //each time to compare and choose the right end
            }else{
                result.add(new Interval(start, end));
                start = interval.start;
                end = interval.end;
            }
        }
        
        result.add(new Interval(start, end));
        return result;
    }
}





//sort the start and end

class Solution {
    public List<Interval> merge(List<Interval> intervals) {
        int n = intervals.size();
        int[] starts = new int[n];
        int[] ends = new int[n];
        
        for(int i=0; i<n;i++){
            starts[i] = intervals.get(i).start;
            ends[i] = intervals.get(i).end;
        }
        
        Arrays.sort(starts);
        Arrays.sort(ends);
        
        List<Interval> results = new ArrayList<>();
        for(int i=0, j=0; i<n; i++){
            if(i==n-1 || starts[i+1] >ends[i]){
                results.add(new Interval(starts[j], ends[i]));
                j = i+1;
            }
        }
        
        return results;
    }
}