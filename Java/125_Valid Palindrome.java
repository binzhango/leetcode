/*
Given a string, determine if it is a palindrome, considering only alphanumeric 
characters and ignoring cases.

For example,
"A man, a plan, a canal: Panama" is a palindrome.
"race a car" is not a palindrome.

Note:
Have you consider that the string might be empty? This is a good question 
to ask during an interview.

For the purpose of this problem, we define empty string as valid palindrome.

*/

class Solution {
    public boolean isPalindrome(String s) {
        int l =0;
        int h = s.length()-1;
        if(s==null || h<1) return true;
        
        while(l<=h){
            // char lc = s.toLowerCase().charAt(l);
            // char hc = s.toLowerCase().charAt(h);
            // if(lc>'z' || lc<'a'){
            //     l++;
            //     continue;
            // }
            // if(hc>'z' || hc<'a'){
            //     h--;
            //     continue;
            // }
            // if(lc==hc){
            //     l++;
            //     h--;
            // }else{
            //     return false;
            // }
            
            char lc = s.charAt(l);
            char hc = s.charAt(h);
            
            if(!Character.isLetterOrDigit(lc)){
                l++;
            }else if(!Character.isLetterOrDigit(hc)){
                h--;
            }else{
                if(Character.toLowerCase(lc) != Character.toLowerCase(hc)){
                    return false;
                }
                l++;
                h--;
            }
            
        }
        return true;
    }
}