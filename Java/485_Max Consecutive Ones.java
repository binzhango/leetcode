/*
Given a binary array, find the maximum number of consecutive 1s in this array.

Example 1:

Input: [1,1,0,1,1,1]
Output: 3
Explanation: The first two digits or the last three digits are consecutive 1s.
    The maximum number of consecutive 1s is 3.
*/

//two pointers used to record the each consecutive 1's, the idea is same to manipulate string
public class Solution {
    public int findMaxConsecutiveOnes(int[] nums) {
        int i=0,j=-1;
        int length=0, max=0;
        if(nums==null || nums.length==0) return 0;
        for(i=0;i<nums.length;i++){
            if(nums[i]==0){
                length = i-j-1;
                j=i;
                max =Math.max(max, length);
            }else{
                if(i==nums.length-1){
                    max=Math.max(max, i-j);
                }
                continue;
            }
        }
        return max;
    }
}

//counting the 1's if we get 0, then set cnt=1;
public class Solution {
    public int findMaxConsecutiveOnes(int[] nums) {
        int max=0;
        int cnt=0;
        for(int i=0;i<nums.length;i++){
            if(nums[i]==1){
                cnt++;
                max =Math.max(max,cnt);
            }else{
                cnt=0;
            }
        }
        return max;
    }
}