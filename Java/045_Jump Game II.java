/*
 Given an array of non-negative integers, you are initially positioned at the first index of the array.
Each element in the array represents your maximum jump length at that position.
Your goal is to reach the last index in the minimum number of jumps.
For example:
Given array A = [2,3,1,1,4]
The minimum number of jumps to reach the last index is 2. (Jump 1 step from index 0 to 1, 
then 3 steps to the last index.) 
*/

public class Solution {
    public int jump(int[] nums) {
    //     int [] dp = new int [nums.length];
    //     dp[0] =0;
    //     for(int i=1 ;i< nums.length;i++){
    //         dp[i] = Integer.MAX_VALUE;
    //         for(int j=0;j<i;j++){
    //             if(i-j<=nums[j]){
    //                 dp[i] = Math.min(dp[i],dp[j]+1);
    //             }
    //         }
    //     }
    //     return dp[nums.length-1];
    
        int count =0;
        int last_jump_max =0;
        int cur_jump_max=0;
        
        for(int i=0;i<nums.length-1;i++){
            cur_jump_max = Math.max(cur_jump_max, i+nums[i]);
            
            if(i==last_jump_max){
                count++;
                last_jump_max = cur_jump_max;
                if(cur_jump_max>=nums.length-1) return count;
            }
        }
    
        return count;
     }
}
