/*
Given a binary tree, return the zigzag level order traversal of its nodes' values.
(ie, from left to right, then right to left for the next level and alternate between).

For example:
Given binary tree [3,9,20,null,null,15,7],

    3
   / \
  9  20
    /  \
   15   7

return its zigzag level order traversal as:

[
  [3],
  [20,9],
  [15,7]
]

*/


/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {
        List<List<Integer>> nestList = new ArrayList<>();
        Queue<TreeNode> queue = new LinkedList<>();
        if(root == null) return nestList;
        queue.offer(root);
        boolean order = true; //flag of direction
        
        while(!queue.isEmpty()){
            int levelNum = queue.size();
            List<Integer> list = new ArrayList<>();
            for(int i=0;i<levelNum;i++){
                if(queue.peek().left!=null) queue.offer(queue.peek().left);
                if(queue.peek().right!=null) queue.offer(queue.peek().right);
                if(order){
                    list.add(queue.poll().val);
                }else{
                    list.add(0, queue.poll().val);
                }
            }
            nestList.add(list);
            order=!order;
            
        }
        return nestList;
        
    }
}