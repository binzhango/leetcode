public class Solution {
    public int nthUglyNumber(int n) {
        if(n==1) return 1;
        int [] dp = new int[n+1];
        dp[1]=1;
        int s2 =1,s3=1,s5=1;
        for(int i=2;i<=n;i++){
            dp[i] = Math.min(2*dp[s2], Math.min(3*dp[s3],5*dp[s5]));
            if(dp[i] == 2*dp[s2]) s2++;
            if(dp[i] == 3*dp[s3]) s3++;
            if(dp[i] == 5*dp[s5]) s5++;
        }
        return dp[n];
    }
}