import java.util.Deque;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.LinkedList;

class TreeNode{
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode(int x){
        val =x;
    }
}
public class TreeTraversal{
    public static void main(String[] args) {
        TreeNode root = new TreeNode(40);
        TreeNode node20 = new TreeNode(20);
        TreeNode node10 = new TreeNode(10);
        TreeNode node30 = new TreeNode(30);
        TreeNode node60 = new TreeNode(60);
        TreeNode node50 = new TreeNode(50);
        TreeNode node70 = new TreeNode(70);
        root.left = node20;
        root.right= node60;

        node20.left = node10;
        node20.right = node30;

        node60.left = node50;
        node60.right = node70;

        TreeTraversal tree = new TreeTraversal();

        // tree.preorder(root);
        // System.out.println();
        // tree.preorderIter(root);
        // System.out.println();
        // tree.inorder(root);
        // System.out.println();
        // tree.inorderIter(root);
        // System.out.println("---post order---");
        // tree.postorder(root);
        // System.out.println();
        // tree.postorderIter(root);
        //System.out.println("---level order---");
        //tree.levelorder(root);
        //System.out.println("---in order---");
        //tree.inorderIter2(root);
        System.out.println("---pos order---");
        //tree.postorderIter(root);
        System.out.println("---seconde---");
        tree.postorderIter2(root);

    }


    /*
     * pre-order
     */
    //recursive 
    public void preorder(TreeNode root){
        if(root!=null){
            System.out.printf("%d ", root.val);
            preorder(root.left);
            preorder(root.right);

        }

    }

    //Iterative 
    public void preorderIter(TreeNode root){
        if(root ==null) return;
        Deque<TreeNode> stack = new ArrayDeque<>();
        stack.push(root);

        while(stack.size()>0){
            TreeNode n = stack.pop();
            System.out.printf("%d ", n.val);

            if(n.right!=null) stack.push(n.right);
            if(n.left!=null) stack.push(n.left);

        }
    }

    public void preorderIter2(TreeNode root){
        Deque<TreeNode> stack = new ArrayDeque<>();
        TreeNode p = root;
        while(p !=null || !stack.isEmpty()){
            if(p!=null) {
                stack.push(p);
                System.out.printf("%d ",p.val);
                p = p.left;
            }else{
                TreeNode node = stack.pop();
                p = node.right;
            }
        }
    }


    /*
    Inorder
    */


    //recursive
    public void inorder(TreeNode root){
        if(root!=null){
            inorder(root.left);
            System.out.printf("%d ", root.val);
            inorder(root.right);
        }
    }
    //iterative

    public void inorderIter(TreeNode root){
        if(root == null) return;

        Deque<TreeNode> stack = new ArrayDeque<>();
        stack.push(root);
        while(stack.size()>0){
            TreeNode temp = stack.peek();
            if(temp.left!=null){
                stack.push(temp.left);
                temp.left =null;
            }else{
                System.out.printf("%d ", temp.val);
                stack.pop();
                if(temp.right!=null){
                    stack.push(temp.right);
                    //temp.right =null;
                }
            }
        }
    }

    public void inorderIter2(TreeNode root){
        Deque<TreeNode> stack = new ArrayDeque<>();
        TreeNode cur = root;
        while(cur!=null || !stack.isEmpty()){
            while(cur!=null){
                stack.push(cur);
                cur=cur.left;
            }
            cur=stack.pop();
            System.out.printf("%d ", cur.val);
            cur = cur.right;
        }
    }

    /*postorder
    */


    //recursion
    public void postorder(TreeNode root){
        if(root!=null){
            postorder(root.left);
            postorder(root.right);
            System.out.printf("%d ", root.val);
        }
    }

    //iterative

    public void postorderIter(TreeNode root){
        if(root ==null) return;

        Deque<TreeNode> stack = new ArrayDeque<>();
        stack.push(root);

        while(stack.size()>0){
            TreeNode temp = stack.peek();
            if(temp.left == null && temp.right ==null){
                TreeNode pop = stack.pop();
                System.out.printf("%d ", pop.val);
            }else{
                if(temp.right !=null){
                    stack.push(temp.right);
                    temp.right=null;
                }
                if(temp.left!=null){
                    stack.push(temp.left);
                    temp.left = null;
                }
            }
        }
        
    }
/*
//alg is wrong
    public void postorderIter2(TreeNode root){
        Deque<TreeNode> stack = new ArrayDeque<>();
        TreeNode p = root;
        while(p!=null || !stack.isEmpty()){
            if(p!=null){
                stack.push(p);
                System.out.printf("%d ", p.val); //print in reverse order
                p = p.right;
            }else{
                TreeNode node = stack.pop();
                p = node.left;
            }
        }
    }
*/

    /*
    level order
    */

    public void levelorder(TreeNode root){
        //Deque<TreeNode> q = new ArrayDeque<>();
        Queue<TreeNode> q = new LinkedList<>();
        q.add(root);
        while(!q.isEmpty()){
            TreeNode n = q.poll();
            System.out.print(n.val+ " ");
            if(n.left!=null) q.add(n.left);
            if(n.right!=null) q.add(n.right);
        }
    }

}
