/*
Given a string S and a string T, find the minimum window in S which will contain all 
the characters in T in complexity O(n).
For example,
S = "ADOBECODEBANC"
T = "ABC"

Minimum window is "BANC". 
*/

public class Solution {
    public String minWindow(String s, String t) {
        int[] table = new int[128];
        for(char c: t.toCharArray()) table[c]++;
        int i=0,j=0;
        int cnt = t.length();
        int min = Integer.MAX_VALUE;
        int start =0;
        while(j<s.length()){
            if(table[s.charAt(j++)]-->0) cnt--;
            while(cnt==0){
                if(j-i<min) min =j-(start=i);
                if(table[s.charAt(i++)]++==0)cnt++;
            }
        }
        
        return min==Integer.MAX_VALUE?"":s.substring(start,start+min);
    }
}


// use template to solve the problem

public class Solution {
    public String minWindow(String s, String t) {
        int[] map = new int[128];
        int begin =0, end =0; // two pointers
        int head =0 ; //
        int min = Integer.MAX_VALUE;
        int count =t.length();
        
        for(char c: t.toCharArray()){
            map[c]++;
        }
        
        while(end<s.length()){
            
            if(map[s.charAt(end)]>0) count--;
            map[s.charAt(end)]--;
            end++;
            
            while(count==0){
                if(end-begin < min) {
                    head = begin;
                    min = end - head;
                    }
                    //min = end-begin;
                    // head = begin;
                if(map[s.charAt(begin)]==0){
                    count++;
                }
                
                map[s.charAt(begin)]++;
                begin++;
            }
        }
        return min==Integer.MAX_VALUE? "": s.substring(head, head+min);
        
        
    }
}



public String minWindow(String s, String t) {
    if(t.length()>s.length()) return "";
    
    //initial window
    Map<Character, Integer> map = new HashMap<>();
    for(char c: t.toCharArray()){
        map.put(c, map.getOrDefault(c,0)+1);
    }
    int count = map.size();
    
    int begin=0;
    int end =0;
    int len = Integer.MAX_VALUE;
    int head =0;
    
    while(end < s.length()){
        char c = s.charAt(end);
        if(map.containsKey(c)){
            map.put(c, map.get(c)-1);
            if(map.get(c)==0) count--;
        }
        end++;
        
        while(count==0){
            char tempc = s.charAt(begin);
            if(map.containsKey(tempc)){
                map.put(tempc, map.get(tempc)+1);
                if(map.get(tempc)>0) count++;
            }
            
            len = Math.min(len, end-begin);
            head =begin;
            begin++;
        }
    }
    if(len == Integer.MAX_VALUE) return "";
    return s.substring(head,head+len);
    
}
