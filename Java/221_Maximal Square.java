/*
 Given a 2D binary matrix filled with 0's and 1's, find the largest
square containing only 1's and return its area.
For example, given the following matrix:
1 0 1 0 0
1 0 1 1 1
1 1 1 1 1
1 0 0 1 0


|x |x x| x
|x |x x| x
     -  -
|x  x x| ?
- - - 
*/

public class Solution {
    public int maximalSquare(char[][] matrix) {
        if(matrix==null || matrix.length==0 || matrix[0].length==0) return 0;
        int m = matrix.length;
        int n = matrix[0].length;
        int [][] dp = new int[m][n];
        int len=0;
        // for(int i=0;i<m;i++){
        //     dp[i][0] = matrix[i][0]-'0';
        //     len = Math.max(len, dp[i][0]);
        // }
        // for(int j=0;j<n;j++){
        //     dp[0][j] = matrix[0][j]-'0';
        //     len = Math.max(len,dp[0][j]);
        // }
        // for(int i=1;i<m;i++){
        //     for(int j=1;j<n;j++){
        //         if(matrix[i][j]=='1'){
        //             dp[i][j] = Math.min(dp[i-1][j-1], Math.min(dp[i-1][j], dp[i][j-1]))+1;
        //         }
        //         len =Math.max(len, dp[i][j]);
        //     }
        // }
        
        
        
        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                if(i==0 || j==0){
                    dp[i][j] = matrix[i][j]-'0';
                }else if(matrix[i][j]=='0'){
                    dp[i][j]=0;
                }else{
                    dp[i][j] = Math.min(dp[i-1][j-1], Math.min(dp[i-1][j], dp[i][j-1]))+1;
                }
                len =Math.max(len, dp[i][j]);
            }
        }
        
        
        
        
        return len*len;
    }
}