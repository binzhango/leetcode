/*
Given a non-empty 2D array grid of 0's and 1's, an island is a group of 1's (representing land) 
connected 4-directionally (horizontal or vertical.) You may assume all four edges of the grid are 
surrounded by water.

Find the maximum area of an island in the given 2D array. (If there is no island, 
the maximum area is 0.)

Example 1:
[[0,0,1,0,0,0,0,1,0,0,0,0,0],
 [0,0,0,0,0,0,0,1,1,1,0,0,0],
 [0,1,1,0,1,0,0,0,0,0,0,0,0],
 [0,1,0,0,1,1,0,0,1,0,1,0,0],
 [0,1,0,0,1,1,0,0,1,1,1,0,0],
 [0,0,0,0,0,0,0,0,0,0,1,0,0],
 [0,0,0,0,0,0,0,1,1,1,0,0,0],
 [0,0,0,0,0,0,0,1,1,0,0,0,0]]
Given the above grid, return 6. Note the answer is not 11, because the island must be connected 4-directionally.
Example 2:
[[0,0,0,0,0,0,0,0]]
Given the above grid, return 0.
Note: The length of each dimension in the given grid does not exceed 50.
*/

class Solution {
    boolean[][] visited; // mask to record whether this island is visited
    public int maxAreaOfIsland(int[][] grid) {
        int row = grid.length;
        int col = grid[0].length;
        visited = new boolean[row][col];
        int max =0;
        
        for(int i=0; i< row; i++){
            for(int j=0; j<col;j++){
                if(grid[i][j]==1 && !visited[i][j]){
                    int area = dfs(grid, i, j, row, col);
                    max = Math.max(max, area);
                }
            }
        }
        return max;
        
    }
    
    public int dfs(int[][] grid, int i, int j, int row, int col){

        if(i<0 || j<0 || i>=row || j>=col || grid[i][j] == 0 || visited[i][j] ) return 0;
        int result =1;
        visited[i][j] = true;
        
        result+= dfs(grid, i-1, j, row, col);
        result+= dfs(grid, i+1, j, row, col);
        result+= dfs(grid, i, j-1, row, col);
        result+= dfs(grid, i, j+1, row, col);
        // for(int dr =-1; dr<=1;dr++){
        //     for(int dc=-1;dc<=1;dc++){
        //         if(dr==0 ^ dc==0){
        //             int nr = i+dr;
        //             int nc = j+dc;
        //             result+= dfs(grid, nr, nc, row, col);
        //         }
        //     }
        // }
        return result;   
    }
}
