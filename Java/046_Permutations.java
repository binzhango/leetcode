
/*
Given a collection of distinct numbers, return all possible permutations.

For example,
[1,2,3] have the following permutations:

[
  [1,2,3],
  [1,3,2],
  [2,1,3],
  [2,3,1],
  [3,1,2],
  [3,2,1]
]

*/


public class Solution {
    public List<List<Integer>> permute(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        backtrack(result, new ArrayList<Integer>(), nums);
        return result;
    }
    
    public void backtrack(List<List<Integer>> result, List<Integer> templist, int[] nums){
        if(templist.size() == nums.length){
            result.add(new ArrayList<>(templist));
        }else{
            for(int i=0; i< nums.length;i++){
                if(templist.contains(nums[i])) continue;
                templist.add(nums[i]);
                backtrack(result, templist, nums);
                templist.remove(templist.size()-1);
            }
        }
    }
}