/*
There are a total of n courses you have to take, labeled from 0 to n - 1.
Some courses may have prerequisites, for example to take course 0 you have
to first take course 1, which is expressed as a pair: [0,1]
Given the total number of courses and a list of prerequisite pairs, is it 
possible for you to finish all courses? 


For example:

2, [[1,0]]

There are a total of 2 courses to take. To take course 1 you should have 
finished course 0. So it is possible.

2, [[1,0],[0,1]]

There are a total of 2 courses to take. To take course 1 you should have finished course
0, and to take course 0 you should also have finished course 1. So it is impossible.

*/


//DFS

public class Solution {
    public boolean canFinish(int numCourses, int[][] prerequisites) {
        if(numCourses ==0 || prerequisites==null || prerequisites.length==0) return true;
        //create ArrayList
        List<List<Integer>> courses = new ArrayList<>(numCourses);
        for(int i=0;i<numCourses;i++){
            courses.add(new ArrayList<Integer>());
        }
        
        //create dependency graph
        for(int i=0; i<prerequisites.length;i++){
            courses.get(prerequisites[i][1]).add(prerequisites[i][0]);
        }
        
        int [] visited = new int[numCourses];
        
        //dfs
        for(int i=0;i<numCourses;i++){
            if(!dfs(i,courses,visited)) return false;
        }
        return true;
    }
    
    public boolean dfs(int index, List<List<Integer>> courses, int[] visited){
        visited[index] = 1;
        
        //get the children list
        List<Integer> results = courses.get(index);
        

        //check each node in the children list
        for(int i=0;i< results.size();i++){
            int result = results.get(i).intValue();
            if(visited[result]==1) return false; //find a visited node => a circle return false
            if(visited[result]==0){
                //recursion the next node
                if(!dfs(result,courses,visited)) return false;
            }
        }
        visited[index] =2; // lable the last node as 2
        return true;
    }
}

