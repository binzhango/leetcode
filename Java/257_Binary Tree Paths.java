/*
 Given a binary tree, return all root-to-leaf paths.

For example, given the following binary tree:

   1
 /   \
2     3
 \
  5

All root-to-leaf paths are:

["1->2->5", "1->3"]
*/

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public List<String> binaryTreePaths(TreeNode root) {
        List<String> ls = new ArrayList<>();
        if(root!=null) helper(root,"", ls);
        return ls;
    }
    public void helper(TreeNode root, String s, List<String> ls){
        if(root.left ==null && root.right==null) ls.add(s+root.val);
        if(root.left!=null) helper(root.left,s+root.val+"->",ls);
        if(root.right!=null) helper(root.right, s+root.val+"->",ls);
    }
}



//BFS
class Solution {
    public List<String> binaryTreePaths(TreeNode root) {
        Deque<TreeNode> q1 = new ArrayDeque<>();
        Deque<String> q2 = new ArrayDeque<>();
        
        List<String> result = new ArrayList<>();
        if(root==null) return result;
        q1.offer(root);
        q2.offer("");
        
        while(!q1.isEmpty()){
            TreeNode node = q1.poll();
            String cur = q2.poll();
            if(node.left==null && node.right==null) result.add(cur+node.val);
            if(node.left!=null){
                q1.offer(node.left);
                q2.offer(cur+node.val+"->");
            }
            if(node.right!=null){
                q1.offer(node.right);
                q2.offer(cur+node.val+"->");
            }
        }
        return result;
    }
}


//DFS
//DFS is fater than BFS

class Solution {
    public List<String> binaryTreePaths(TreeNode root) {
        Deque<TreeNode> q1 = new ArrayDeque<>();
        Deque<String> q2 = new ArrayDeque<>();
        
        List<String> result = new ArrayList<>();
        if(root==null) return result;
        q1.push(root);
        q2.push("");
        
        while(!q1.isEmpty()){
            TreeNode node = q1.pop();
            String cur = q2.pop();
            if(node.left==null && node.right==null) result.add(cur+node.val);
            if(node.left!=null){
                q1.push(node.left);
                q2.push(cur+node.val+"->");
            }
            if(node.right!=null){
                q1.push(node.right);
                q2.push(cur+node.val+"->");
            }
        }
        return result;
    }
}