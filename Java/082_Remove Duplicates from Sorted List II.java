/*
 Given a sorted linked list, delete all nodes that have duplicate numbers,
leaving only distinct numbers from the original list.

For example,
Given 1->2->3->3->4->4->5, return 1->2->5.
Given 1->1->1->2->3, return 2->3. 
*/

/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class Solution {
    public ListNode deleteDuplicates(ListNode head) {
        // ListNode dummy = new ListNode(0);
        // ListNode temp = head;
        // Map<Integer, Integer> map = new HashMap<>();
        // if(head==null || head.next ==null) return head;
        // while(temp!=null){
        //     if(map.containsKey(temp.val)){
        //         map.put(temp.val, map.get(temp.val)+1);
        //     }else{
        //         map.put(temp.val,1);
        //     }
        //     temp=temp.next;
        // }
        // ListNode cur = head;
        // dummy.next =cur;
        // while(head!=null){
        //     int count = map.get(head.val);
        //     if(count==1) cur.next = head;
        //     head=head.next;
        //     cur =cur.next;
        // }
        // return dummy.next;
        if(head==null || head.next==null) return head;
        ListNode dummy = new ListNode(0==head.val?1:0);
        
        ListNode pre = dummy;
        ListNode cur =head;
        dummy.next = cur;
        ListNode first = dummy;
        while(cur!=null && cur.next!=null){
            if(pre.val!=cur.val && cur.val!=cur.next.val){
                first.next = cur;
                first = first.next;
            }
            pre = cur;
            cur = cur.next;
        }
        if(cur.next == null){
            if(pre.val != cur.val){
                first.next = cur;
                first = first.next;
            }
        }
        first.next =null;
        return dummy.next;
    }
}
