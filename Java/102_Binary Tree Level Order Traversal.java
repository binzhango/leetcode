/*
Given a binary tree, return the level order traversal of its nodes' values. (ie, from left to right,
level by level).

For example:
Given binary tree [3,9,20,null,null,15,7],
    3
   / \
  9  20
    /  \
   15   7

 return its level order traversal as:

[
  [3],
  [9,20],
  [15,7]
]

*/

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Solution {
    public List<List<Integer>> levelOrder(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        List<List<Integer>> nestList = new LinkedList<>();
        if(root == null) return nestList;
        queue.offer(root); //insert the root to the tail of the queue
        while(!queue.isEmpty()){
            int levelNum = queue.size();
            List<Integer> list = new LinkedList<>();
            for(int i = 0; i<levelNum;i++){
                if(queue.peek().left!=null) queue.offer(queue.peek().left); //update the queue by adding the left child
                if(queue.peek().right!=null) queue.offer(queue.peek().right); // update the queue by adding the right child
                list.add(queue.poll().val); // update the queue by removing the node and put the node into list
            }
            nestList.add(list);
        }
        return nestList;
    }
}