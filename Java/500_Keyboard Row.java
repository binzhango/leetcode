/*
Given a List of words, return the words that can be typed using letters of alphabet on 
only one row's of American keyboard like the image below. 

Example 1:

Input: ["Hello", "Alaska", "Dad", "Peace"]
Output: ["Alaska", "Dad"]

*/

public class Solution {
    public String[] findWords(String[] words) {
        if(words == null) return words;
        String [] keys = {"qwertyuiop","asdfghjkl","zxcvbnm"};
        Map<Character, Integer> map = new HashMap<>();
        for(int i=0; i< keys.length;i++){
            for(char c: keys[i].toCharArray()){
                map.put(c,i);
            }
        }
        List<String> result = new ArrayList<>();
        for(String s : words){
            if(s.equals("") || s==null) continue;
            int index = map.get(s.toLowerCase().charAt(0));
            char[] temp = s.toLowerCase().toCharArray();

            for(char c:s.toLowerCase().toCharArray()){
                if(map.get(c)!=index){
                    index =-1;
                    break;
                }
            }
            
            if(index !=-1) result.add(s);
        }
        
        return result.toArray(new String[0]);                
    }
}



class Solution {
    public String[] findWords(String[] words) {
       return Stream.of(words).filter(s-> s.toLowerCase().matches("[qwertyuiop]*|[asdfghjkl]*|[zxcvbnm]*")).toArray(String[]::new);
    }
}