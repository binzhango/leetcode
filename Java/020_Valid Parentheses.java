public class Solution {
    public boolean isValid(String s) {
        ArrayDeque<Character> ad = new ArrayDeque<>();
        for(char c:s.toCharArray()){
            if(c=='(') ad.push(')');
            else if(c=='{') ad.push('}');
            else if(c=='[') ad.push(']');
            else if(ad.isEmpty() || ad.pop()!=c)
            return false;
        }
        return ad.isEmpty();
    }
}
