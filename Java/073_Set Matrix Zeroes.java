/*
Given a m x n matrix, if an element is 0, 
set its entire row and column to 0. Do it in place. 
*/

public class Solution {
    public void setZeroes(int[][] matrix) {
        int r = matrix.length;
        int c = matrix[0].length;
        boolean [] rb = new boolean[r];
        boolean [] cb = new boolean[c];
        for(int i=0;i<r;i++){
            for(int j=0;j<c;j++){
                if(matrix[i][j]==0){
                    rb[i] = true;
                    cb[j] = true;
                }
            }
        }
        
        for(int i=0;i<r;i++){
            for(int j=0;j<c;j++){
                if(rb[i] == true || cb[j] == true)
                matrix[i][j]=0;
            }
        }
        
    }
}
