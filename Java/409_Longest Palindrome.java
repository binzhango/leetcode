/*
Given a string which consists of lowercase or uppercase letters,
find the length of the longest palindromes that can be built with those letters.
This is case sensitive, for example "Aa" is not considered a palindrome here.
Example:
Input:
"abccccdd"
Output:
7
Explanation:
One longest palindrome that can be built is "dccaccd", whose length is 7.
*/

public class Solution {
    public int longestPalindrome(String s) {
        if(s==null || s.length()==0) return 0;
        boolean oddOne = false;
        int len =0;
        int n= s.length();
        Map<Character,Integer> map = new HashMap<Character,Integer>();
        //char[] cs = new char[n];
        char[] cs = s.toCharArray();
        for(int i=0;i<n;i++){
            if(map.containsKey(cs[i])){
                map.put(cs[i],map.get(cs[i])+1);
            }else{
                map.put(cs[i],1);
            }
        }
        
        for(Map.Entry<Character,Integer> me : map.entrySet()){
            int num = me.getValue();
            if(num%2 ==0){
                len+=num;
            }else{
                len=len+num-1;
                oddOne = true;
            }
        }
        return len+(oddOne? 1:0);
        
    }


        public static int longestPalindrome2(String s){
        if(s==null || s.length()==0) return 0;
        int count =0;
        Set<Character> set = new HashSet<Character>();
        for(int i=0;i<s.length();i++){
            if(set.contains(s.charAt(i))){
                count+=2;
                set.remove(s.charAt(i));
            }else{
                set.add(s.charAt(i));
            }
        }
        return count+(set.isEmpty()? 0:1);
    }
}