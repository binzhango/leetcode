/*
Follow up for "Search in Rotated Sorted Array":
What if duplicates are allowed?

Would this affect the run-time complexity? How and why?

Write a function to determine if a given target is in the array.
*/

public class Solution {
    public boolean search(int[] nums, int target) {
        int l =0;
        int r = nums.length-1;
        while(l<=r){
            int m =l+(r-l)/2;
            if(nums[m]==target) return true;
            if(nums[l]==nums[m] && nums[m]==nums[r]){
                l++;
                r--;
            }else if(nums[m]>=nums[l]){
                if(target<nums[m] && nums[l]<=target){
                    r=m-1;
                }else{
                    l=m+1;
                }
            }else if(nums[m]<=nums[r]){
                if(target>nums[m] && target<=nums[r]){
                    l=m+1;
                }else{
                    r=m-1;
                }
            }
            
        }
        return false;
    }
}
