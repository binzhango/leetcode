/*
There are two sorted arrays nums1 and nums2 of size m and n respectively.
Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
Example 1:
nums1 = [1, 3]
nums2 = [2]
The median is 2.0
Example 2:
nums1 = [1, 2]
nums2 = [3, 4]
The median is (2 + 3)/2 = 2.5

*/


/*
m<n
A[0] A[1] ... A[i-1] | A[i] A[i+1] ... A[m]
B[0] B[1] ... B[j-1] | B[j] B[j+1] ... B[n]
-------maxL---------  ---------minR--------


A[i-1] < B[j] && B[j-1] < A[i] --> i is the index we need --> max(A[0-i], B[0-j]) is the median

if A[i-1] > B[j] && i>0 --> i is large -> index=i-1;
if A[i] < B[j-1] && i<m --> i is small -> index = i+1;


find the right i -->
i=0 --> maxL=B[j-1]
j=0 --> maxL = A[i-1]
maxL = max(A[i-1], B[j-1])

i=m --> minR = B[j]
j=n --> minR = A[i]
minR = min(A[i], B[j])

*/




public class Solution {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int m = nums1.length;
        int n = nums2.length;
        if(m>n) return findMedianSortedArrays(nums2, nums1);
        int iMin =0, iMax = m;
        int maxL =0, minR=0;
        int i=0, j=0;
        while(iMin<=iMax){
            i = (iMin+iMax)/2;
            j= (m+n+1)/2-i;
            if(i>0  && nums1[i-1]>nums2[j]){
                iMax=i-1;
            }else if(i<m && nums1[i]<nums2[j-1]){
                iMin=i+1;
            }else{
                if(i==0) maxL = nums2[j-1];
                else if(j==0) maxL = nums1[i-1];
                else maxL = Math.max(nums1[i-1],nums2[j-1]);
                
                if((m+n)%2==1) return maxL;
                
                if(i==m) minR = nums2[j];
                else if(j==n) minR = nums1[i];
                else minR = Math.min(nums1[i], nums2[j]);
                
                return (maxL+ minR)/2.0;
            }
        }
        return -1;
    }
}
