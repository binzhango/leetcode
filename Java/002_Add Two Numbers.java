/*
You are given two linked lists representing two non-negative numbers.
The digits are stored in reverse order and each of their nodes contain a single digit.
 Add the two numbers and return it as a linked list.

Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8
*/


/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode result = new ListNode(0);
        ListNode p =l1;
        ListNode q =l2;
        int carry = 0;
        ListNode head = result; // point to the current node
        while(p !=null || q!=null){
            //use x, y to get the node value especially the null node, don't need to check node every time
            int x = (p!=null) ? p.val:0;
            int y = (q!=null) ? q.val:0;
            int sum = x+y+carry;
            //update the carry and add a new node to head
            carry = sum/10;
            head.next = new ListNode(sum%10);
            //move the list to next
            head = head.next;
            if(p!=null) p = p.next;
            if(q!=null) q = q.next;
        }
        //if carry is not 0 , we need to append a new node to the end to list
        if(carry!=0) head.next = new ListNode(carry);
        return result.next;
    }
}
