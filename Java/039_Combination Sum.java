/*
 Given a set of candidate numbers (C) (without duplicates) and a target number (T), 
find all unique combinations in C where the candidate numbers sums to T.
The same repeated number may be chosen from C unlimited number of times. 

Note:

    All numbers (including target) will be positive integers.
    The solution set must not contain duplicate combinations.

For example, given candidate set [2, 3, 6, 7] and target 7,
A solution set is:

[
  [7],
  [2, 2, 3]
]

*/


public class Solution {
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        List<List<Integer>> result = new ArrayList<>();
        Arrays.sort(candidates);
        backtrack(result, new ArrayList<Integer>(), candidates, target, 0);
        return result;
    }
    
    public void backtrack(List<List<Integer>> result, List<Integer> templist, int[] nums, int remain, int start){
        if(remain<0){
            return;
        }else if(remain ==0){
            result.add(new ArrayList<Integer>(templist));
        }else{
            for(int i= start; i< nums.length; i++){
                templist.add(nums[i]);
                backtrack(result, templist, nums, remain-nums[i], i);
                templist.remove(templist.size()-1);
            }
        }
    }
}