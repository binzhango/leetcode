/*
Serialization is the process of converting a data structure or object into a sequence of bits 
so that it can be stored in a file or memory buffer, or transmitted across a network connection 
link to be reconstructed later in the same or another computer environment.

Design an algorithm to serialize and deserialize a binary search tree. There is no restriction 
on how your serialization/deserialization algorithm should work. You just need to ensure that 
a binary search tree can be serialized to a string and this string can be deserialized to the 
original tree structure.

The encoded string should be as compact as possible. 
*/
/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */
public class Codec {

    // Encodes a tree to a single string.
    public String serialize(TreeNode root) {
        if(root==null) return null;
        String left = serialize(root.left);
        String right = serialize(root.right);
        if(left==null && right==null) return root.val+"";
        
        StringBuilder sb = new StringBuilder();
        sb.append(root.val);
        
        if(left!=null) sb.append(","+left);
        if(right!=null) sb.append(","+right);
        return sb.toString();
    }

    // Decodes your encoded data to tree.
    public TreeNode deserialize(String data) {
        if(data==null) return null;
        
        String[] s = data.split(",");
        int[] nodes = new int[s.length];
        int i=0;
        for(String node:s){
            nodes[i++] = Integer.parseInt(node);
        }
        return deserialize(nodes, 0, nodes.length-1);
    }
    
    public TreeNode deserialize(int[] nodes, int start, int end){
        if(start>end) return null;
        if(start==end) return new TreeNode(nodes[start]);
        int leftEnd = start, rightStart=start+1;
        TreeNode root = new TreeNode(nodes[start]);
        
        for(int i=start+1;i<=end;i++){
            if(root.val<nodes[i]) break;
            leftEnd=i;
            rightStart= leftEnd+1;
        }
        root.left = deserialize(nodes, start+1, leftEnd);
        root.right = deserialize(nodes, rightStart, end);
        return root;
    }
    
    
}

// Your Codec object will be instantiated and called as such:
// Codec codec = new Codec();
// codec.deserialize(codec.serialize(root));