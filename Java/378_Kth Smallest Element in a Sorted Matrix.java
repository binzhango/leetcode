/*
Given a n x n matrix where each of the rows and columns are sorted in ascending order,
find the kth smallest element in the matrix.
Note that it is the kth smallest element in the sorted order, not the kth distinct element.

Example:

matrix = [
   [ 1,  5,  9],
   [10, 11, 13],
   [12, 13, 15]
],
k = 8,

return 13.
 
*/


public class Solution {
    public int kthSmallest(int[][] matrix, int k) {
        PriorityQueue<int[]> pq = new PriorityQueue<>(new Comparator<int[]>(){
            @Override
            public int compare(int[] a, int[] b){
                return a[0]-b[0];
            }
        });
        
        if(matrix.length==0 || k==0){
            return -1;
        }
        pq.offer(new int[]{matrix[0][0],0,0});
        int[] peak = new int[3];
        while(k-->0){
            peak = pq.poll();
            if(peak[1]+1<matrix.length){
                pq.offer(new int[]{matrix[peak[1]+1][peak[2]],peak[1]+1,peak[2]});
            }
            if(peak[1]==0&&peak[2]+1<matrix[0].length){
                pq.offer(new int[]{matrix[peak[1]][peak[2]+1],peak[1],peak[2]+1});
            }
        }
        return peak[0];
        
    }
}