/*Given an integer matrix, find the length of the longest increasing path.
From each cell, you can either move to four directions: left, right, up or down. 
You may NOT move diagonally or move outside of the boundary (i.e. wrap-around is not allowed).
Example 1:
nums = [
  [9,9,4],
  [6,6,8],
  [2,1,1]
]
Return 4
The longest increasing path is [1, 2, 6, 9].

Example 2:
nums = [
  [3,4,5],
  [3,2,6],
  [2,2,1]
]
Return 4
The longest increasing path is [3, 4, 5, 6]. Moving diagonally is not allowed.

*/





/*
1.Do DFS from every cell
2.Compare every 4 direction and skip cells that are out of boundary or smaller
3.Get matrix max from every cell's max
4.Use matrix[x][y] <= matrix[i][j] so we don't need a visited[m][n] array
5.The key is to cache the distance because it's highly possible to revisit a cell


*/
public class Solution {
  public final static int[][] dirs = { { 0, 1 }, { 1, 0 }, { 0, -1 }, { -1, 0 } };

  public int longestIncreasingPath(int[][] matrix) {
    if (matrix.length == 0 || matrix == null)
      return 0;
    int m = matrix.length, n = matrix[0].length;
    int[][] lens = new int[m][n];
    int max = 1;
    for (int i = 0; i < m; i++) {
      for (int j = 0; j < n; j++) {
        int len = dfs(matrix, i, j, m, n, lens);
        max = Math.max(max, len);
      }
    }
    return max;
  }

  /**
  *@param  i and j are indice of the element in array
  *@param  m and n are the sizes of the array
  */
  public static int dfs(int[][] matrix, int i, int j, int m, int n, int[][] lens) {
    if (lens[i][j] != 0)
      return lens[i][j];
    int max = 1;
    for (int[] dir : dirs) {
      int x = i + dir[0], y = j + dir[1];
      if (x < 0 || y < 0 || x >= m || y >= n || matrix[x][y] <= matrix[i][j])
        continue;
      int len = 1 + dfs(matrix, x, y, m, n, lens);
      max = Math.max(max, len);
    }
    lens[i][j] = max;
    return max;
  }
}