/*
Given a non-empty 2D array grid of 0's and 1's, an island is a group of 1's 
(representing land) connected 4-directionally (horizontal or vertical.) You may assume 
all four edges of the grid are surrounded by water.

Count the number of distinct islands. An island is considered to be the same as another 
if and only if one island can be translated (and not rotated or reflected) to equal the other.


Example 1:
11000
11000
00011
00011
Given the above grid map, return 1.
Example 2:
11011
10000
00001
11011
Given the above grid map, return 3.

Notice that:
11
1
and
 1
11
are considered different island shapes, because we do not consider reflection / rotation.


*/



class Solution {
    public int numDistinctIslands(int[][] grid) {
        Set<String> result = new HashSet<>();
        for(int i=0;i<grid.length;i++){
            for(int j=0;j<grid[i].length;j++){
                String s = dfs(grid, i, j);
                if(!s.equals("")) result.add(s);
            }
        }
        return result.size();
    }
    
    public String dfs(int[][] grid, int i, int j){
        if(grid[i][j]==0) return "";
        grid[i][j] =0;
        String result ="K";
        if(i>0){
            String s = dfs(grid, i-1,j);
            if(s.length()>0) result+=s+"L";
        }
        
        if(i<grid.length-1){
            String s = dfs(grid, i+1,j);
            if(s.length()>0) result+=s+"R";
        }
        
        if(j>0){
            String s = dfs(grid, i, j-1);
            if(s.length()>0) result+=s+"U";
        }
        
        if(j<grid[0].length-1){
            String s = dfs(grid, i, j+1);
            if(s.length()>0) result+=s+"D";
        }
        return result;
    }
}
