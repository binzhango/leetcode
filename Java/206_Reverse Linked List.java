/*
Reverse a singly linked list.
*/


/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */



 //recursive
class Solution {
    public ListNode reverseList(ListNode head) {
        return reverseHelp(head, null);
    }
    
    public ListNode reverseHelp(ListNode head, ListNode temp){
        if(head== null) return temp;
        ListNode next = head.next;
        head.next = temp;
        return reverseHelp(next, head);
        
    }
}


//iteration
class Solution {
    public ListNode reverseList(ListNode head) {
        ListNode temp = null;
        while(head !=null){
            ListNode next = head.next;
            head.next = temp;
            temp = head;
            head = next;
        }
        return temp;
    }
}
