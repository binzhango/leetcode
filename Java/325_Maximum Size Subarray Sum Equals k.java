/*
Given an array nums and a target value k, find the maximum length of a subarray that sums to k. If there isn't one, return 0 instead.

Note:
The sum of the entire nums array is guaranteed to fit within the 32-bit signed integer range.

Example 1:
Given nums = [1, -1, 5, -2, 3], k = 3,
return 4. (because the subarray [1, -1, 5, -2] sums to 3 and is the longest)

Example 2:
Given nums = [-2, -1, 2, 1], k = 1,
return 2. (because the subarray [-1, 2] sums to 1 and is the longest)

Follow Up:
Can you do it in O(n) time?

*/

//brute force O(n^3)

class Solution {
    public int maxSubArrayLen(int[] nums, int k) {
        int max=0;
        for(int i=0;i<nums.length;i++){
            for(int j=0;j<=i;j++){
                int temp =0;
                temp = sum(nums, i, j);
                if(temp==k){
                    max = Math.max(max, i-j+1);
                }
                
            }
        }
        return max;
        
    }
    
    public int sum(int[] nums, int i ,int j){
        int result =0;
        while(j<=i){
            result += nums[j];
            j++;
        }
        return result;
    }
}

//dp memory limited

class Solution {
    public int maxSubArrayLen(int[] nums, int k) {
        if(nums==null || nums.length==0) return 0;
        int max=0;
        int[][] dp = new int[nums.length][nums.length];
        dp[0][0]=nums[0];
        if(dp[0][0]==k) max=1;
        
        for(int i=1;i<nums.length;i++){
            for(int j=0; j<=i;j++){
                dp[i][j] = i==j? nums[j]:dp[i][j-1]+nums[j];
                if(dp[j][i]==k) max= Math.max(max, i-j+1);
            }
        }
        return max;       
    }
}

//hashmap

class Solution {
    public int maxSubArrayLen(int[] nums, int k) {
        
        if(nums==null || nums.length==0) return 0;
        Map<Integer, Integer> map = new HashMap<>();
        int sum =0;
        int max=0;
        for(int i=0;i<nums.length;i++){
            sum+= nums[i];
            if(sum==k) max = i+1;
            else if(map.containsKey(sum-k)) max = Math.max(max, i-map.get(sum-k));
            if(!map.containsKey(sum)) map.put(sum, i);
        }
        return max;
    }
}