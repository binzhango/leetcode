/*
All DNA is composed of a series of nucleotides abbreviated as A, C, G, and T,
for example: "ACGAATTCCG". When studying DNA, it is sometimes useful to identify
repeated sequences within the DNA.
Write a function to find all the 10-letter-long sequences (substrings) that occur
more than once in a DNA molecule.

For example,

Given s = "AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT",

Return:
["AAAAACCCCC", "CCCCCAAAAA"].

*/



public class Solution {
    public List<String> findRepeatedDnaSequences(String s) {
        Set results = new HashSet();
        Set repeated = new HashSet();
        
        for(int i=0;i+9<s.length();i++){
            String temp = s.substring(i,i+10);
            if(!results.add(temp)){ //add() method will return a boolean if add successfully
                                    //add() = first check it contains or not and then add it or not
                repeated.add(temp);
            }
        }
        
        return new ArrayList(repeated);
    }
}