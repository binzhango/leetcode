/*Given a string, find the length of the longest substring without repeating characters.

Examples:
Given "abcabcbb", the answer is "abc", which the length is 3.
Given "bbbbb", the answer is "b", with the length of 1.
Given "pwwkew", the answer is "wke", with the length of 3. Note that the answer must be a substring, 
"pwke" is a subsequence and not a substring.
*/

public class Solution {
    public int lengthOfLongestSubstring(String s) {
        //greedy algorithm
        if(s.length()==0 || s==null ) return 0;
        Map<Character, Integer> map = new HashMap<>();
        char[] ch = s.toCharArray();
        int max=0;
        for(int i=0, j=0;i<s.length();i++){
            if(map.containsKey(ch[i])){
                j= Math.max(j,map.get(ch[i])+1); // if find the same char, and get the position
            }
            map.put(ch[i],i);//find the last position 
            max = Math.max(max,i-j+1);  //calculate max everytime after update i
        }
        return max;
    }
}

//another methode 
//use the template to solve the substring problem

public int lengthOfLongestSubstring(String s) {
    if (s == null || s.length() == 0) {
        return 0;
    }
    int len = 0;
    int head = 0, i = 0;
    int[] sTable = new int[256];
    int repeat = 0;
    while (i < s.length()) {
        if (sTable[s.charAt(i++)]++ > 0) repeat++;   //total number of repeat
        while(repeat > 0) {
           if (sTable[s.charAt(head++)]-- > 1) repeat--;
        }
        len = Math.max(len, i - head);
    }
    return len;
}
