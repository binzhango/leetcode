/*
 The thief has found himself a new place for his thievery again. 
There is only one entrance to this area, called the "root."
Besides the root, each house has one and only one parent house. 
After a tour, the smart thief realized that "all houses in this place forms a binary tree". 
It will automatically contact the police if two directly-linked houses were broken into on the same night.
Determine the maximum amount of money the thief can rob tonight without alerting the police. 

Example 1:
     3
    / \
   2   3
    \   \ 
     3   1

Maximum amount of money the thief can rob = 3 + 3 + 1 = 7. 

Example 2:
     3
    / \
   4   5
  / \   \ 
 1   3   1

 Maximum amount of money the thief can rob = 4 + 5 = 9. 

*/

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */

/*
------------------recursion--------------------------

slowly!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/

public class Solution {
    public int rob(TreeNode root) {
        if (root == null)
            return 0;
        if (root.left == null && root.right == null)
            return root.val;
        // int include = root.val+ rob(root.left.left)+rob(root.left.right) + rob(root.right.left)+rob(root.right.right);
        // int exclude = rob(root.left)+rob(root.right);
        int include = root.val;
        int exclude = 0;
        if (root.left != null) {
            include += rob(root.left.left) + rob(root.left.right);
            exclude += rob(root.left);
        }
        if (root.right != null) {
            include += rob(root.right.left) + rob(root.right.right);
            exclude += rob(root.right);
        }

        return Math.max(include, exclude);

    }
}

/*
------------------------------Dynamic Programming--------------------------
*/

public class Solution {
    public int rob(TreeNode root) {
        return robDFS(root)[1];
    }

    private int[] robDFS(TreeNode node) {
        if (node == null)
            return new int[] { 0, 0 };
        int[] state = new int[2]; //state[0] : do not rob this node
                                  //state[1] : max_value of this node
        int[] l = robDFS(node.left);
        int[] r = robDFS(node.right);
        state[0] = l[1] + r[1];
        state[1] = Math.max(l[0] + r[0] + node.val, state[0]);
        return state;
    }

}



class Solution {
    public int rob(TreeNode root) {
        int[] result = robDFS(root); //result[0] not contain
                                    // result[1] contain
        return Math.max(result[0], result[1]);
    }
    
    public int[] robDFS(TreeNode root){
        if(root==null) return new int[2];
        int[] left = robDFS(root.left);
        int[] right = robDFS(root.right);
        int[] result = new int[2];
        result[0] = Math.max(left[0],left[1]) + Math.max(right[0], right[1]);
        result[1] = root.val+left[0]+right[0];
        
        return result;
    }
}