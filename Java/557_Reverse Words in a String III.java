/*
Given a string, you need to reverse the order of characters in each word within 
a sentence while still preserving whitespace and initial word order.

Example 1:

Input: "Let's take LeetCode contest"
Output: "s'teL ekat edoCteeL tsetnoc"

*/

public class Solution {
    public String reverseWords(String s) {
        String[] ss = s.split(" ");
        StringBuilder sb =new StringBuilder();
        for(int i=0;i<ss.length;i++){
            if(i==ss.length-1) {
                sb.append(help(ss[i]));
            }else{
                sb.append(help(ss[i])).append(" ");
            }
            
        }
        return sb.toString();
    }
    
    public String help(String s){
        char[] cs = s.toCharArray();
        StringBuilder sb = new StringBuilder();
        for(int i=cs.length-1;i>=0;i--){
            sb.append(cs[i]);
        }
        return sb.toString();
    }
}



public class Solution {
    public String reverseWords(String s) {
        String[] ss = s.split(" ");
        StringBuilder sb =new StringBuilder();
        StringBuilder temp = new StringBuilder();
        for(int i=0;i<ss.length;i++){
            if(i==ss.length-1) {
                sb.append(new StringBuilder(ss[i]).reverse().toString());
            }else{
                sb.append(new StringBuilder(ss[i]).reverse().toString()).append(" ");
            }
        }
        return sb.toString();
    }
}