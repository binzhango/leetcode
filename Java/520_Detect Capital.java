/*
 Given a word, you need to judge whether the usage of capitals in it is right or not.

We define the usage of capitals in a word to be right when one of the following cases holds:

    All letters in this word are capitals, like "USA".
    All letters in this word are not capitals, like "leetcode".
    Only the first letter in this word is capital if it has more than one letter, like "Google".

Otherwise, we define that this word doesn't use capitals in a right way.

Example 1:

Input: "USA"
Output: True

Example 2:

Input: "FlaG"
Output: False

*/


public class Solution1 {
    public boolean detectCapitalUse(String word) {
        char [] cs = word.toCharArray();
        if(cs.length<=1) return true;
        if(cs[0]>='a' && cs[0]<='z'){
            for(int i=1;i<cs.length;i++){
                if(cs[i]>='A' && cs[i]<='Z'){
                    return false;
                }
            }
        }else{
            if(cs[1]>='a' && cs[1]<='z'){
                for(int i=2;i<cs.length;i++){
                    if(cs[i]>='A' && cs[i]<='Z'){
                        return false;
                    }
                }
            }else{
                for(int i=2;i<cs.length;i++){
                    if(cs[i]>='a' && cs[i]<='z'){
                        return false;
                    }
                }
            }
        }
        return true;
    }
}


public class Solution2{
    public boolean detectCapitalUse(String word){
        return word.equals(word.toUpperCase()) || 
               word.equals(word.toLowerCase()) ||
               (Character.isUpperCase(word.charAt(0)) && 
               word.substring(1).equals(word.substring(1).toLowerCase()));
    }
}


public class Solution3{
    public boolean detectCapitalUse(String word) {
        return word.matches("[A-Z]+|[a-z]+|[A-Z][a-z]+");
    }
}