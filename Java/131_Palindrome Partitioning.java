/*
Given a string s, partition s such that every substring of the partition is 
a palindrome.

Return all possible palindrome partitioning of s.

For example, given s = "aab",
Return

[
  ["aa","b"],
  ["a","a","b"]
]
*/


class Solution {
    public List<List<String>> partition(String s) {
        List<List<String>> list =new ArrayList<>();
        backtrack(list, new ArrayList<String>(), s, 0);
        return list;
    }
    
    public void backtrack(List<List<String>> list, List<String> temp, String s, int start){
        if(start==s.length()){ //size match add temp
            list.add(new ArrayList<String>(temp));
        }else{
            for(int i= start;i<s.length();i++){
                if(isPalindrome(s,start,i)){
                    temp.add(s.substring(start,i+1));
                    backtrack(list, temp, s, i+1);
                    temp.remove(temp.size()-1);
                }
            }
        }
    }
    
    public boolean isPalindrome(String s, int l, int r){
        if(l==r) return true;
        while(l<r){
            if(s.charAt(l) != s.charAt(r)) return false;
            l++;
            r--;
        }
        return true;
    }
    
}