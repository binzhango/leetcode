/*
Median is the middle value in an ordered integer list. If the size of the list is even, 
there is no middle value. So the median is the mean of the two middle value.
Examples:

[2,3,4] , the median is 3

[2,3], the median is (2 + 3) / 2 = 2.5

Design a data structure that supports the following two operations:

    void addNum(int num) - Add a integer number from the data stream to the data structure.
    double findMedian() - Return the median of all elements so far.

For example:
add(1)
add(2)
findMedian() -> 1.5
add(3) 
findMedian() -> 2
*/

public class MedianFinder {
    private PriorityQueue<Integer> maxheap = new PriorityQueue<>(Collections.reverseOrder());
    private PriorityQueue<Integer> minheap = new PriorityQueue<>();
    // Adds a number into the data structure.
    public void addNum(int num) {
        minheap.offer(num);
        maxheap.offer(minheap.poll());
        if(maxheap.size()>minheap.size()){
            minheap.offer(maxheap.poll());
        }
    }

    // Returns the median of current data stream
    public double findMedian() {
        return minheap.size()>maxheap.size()? (double)minheap.peek() : (minheap.peek()+maxheap.peek())/2.0;
    }
};

// Your MedianFinder object will be instantiated and called as such:
// MedianFinder mf = new MedianFinder();
// mf.addNum(1);
// mf.findMedian();