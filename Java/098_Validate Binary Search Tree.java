/*

Given a binary tree, determine if it is a valid binary search tree (BST).

Assume a BST is defined as follows:

The left subtree of a node contains only nodes with keys less than the node's key.
The right subtree of a node contains only nodes with keys greater than the node's key.
Both the left and right subtrees must also be binary search trees.
Example 1:
    2
   / \
  1   3
Binary tree [2,1,3], return true.
Example 2:
    1
   / \
  2   3
Binary tree [1,2,3], return false.
*/

/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode(int x) { val = x; }
 * }
 */


 //recursive
class Solution {
    public boolean isValidBST(TreeNode root) {
//         if(root==null) return true;
//         boolean lb = false;
//         boolean rb =false;
//         if(root.left!=null){
//             lb= root.val>root.left.val? true:false;
//         }else{
//             lb =true;
//         }
//         if(root.right!=null){
//             rb = root.val<root.right.val? true:false;
//         } else{
//             rb =true;
//         }
        
//         return lb && rb && isValidBST(root.left) && isValidBST(root.right);
        
        return help(root, null, null);
    }
    
    //help function -- two integers
    //min -- left branch shoule greater than min and less than root
    //max -- right branch should less than max and greater than root

    public boolean help(TreeNode root, Integer min, Integer max){
        if(root==null) return true;
        if((min!=null && root.val<=min) || (max!=null && root.val>=max)){
            return false;
        }
        return help(root.left, min, root.val) && help(root.right, root.val, max);
           
                
    }
}