/*
Given a collection of numbers that might contain duplicates, 
return all possible unique permutations.

For example,
[1,1,2] have the following unique permutations:

[
  [1,1,2],
  [1,2,1],
  [2,1,1]
]


*/

//use boolean[] as a flag to check the element is used or not.

public class Solution {
    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        Arrays.sort(nums);
        backtrack(result, new ArrayList<>(), nums, new boolean[nums.length]);
        return result;
    }
    
    public void backtrack(List<List<Integer>> result, List<Integer> templist, int[] nums, boolean[] used){
        if(templist.size() == nums.length){
            result.add(new ArrayList<>(templist));
        }else{
            for(int i=0;i<nums.length;i++){
                //current is not used and previous is used and they are same then add this value
                if(used[i]||i>0 && nums[i]==nums[i-1]&& !used[i-1]) continue;
                used[i] = true;
                templist.add(nums[i]);
                backtrack(result, templist, nums, used);
                used[i]=false;
                templist.remove(templist.size()-1);
            }
        }
    }
}