"""
Given n nodes labeled from 0 to n - 1 and a list of undirected edges 
(each edge is a pair of nodes), write a function to check whether these edges make up a valid tree.

For example:

Given n = 5 and edges = [[0, 1], [0, 2], [0, 3], [1, 4]], return true.

Given n = 5 and edges = [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]], return false.

Note: you can assume that no duplicate edges will appear in edges. 
Since all edges are undirected, [0, 1] is the same as [1, 0] and thus will not 
appear together in edges.
"""


class Solution(object):
    def validTree(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: bool
        """
        #BFS
        dic = {i:set() for i in range(n)}
        for i, j in edges:
            dic[i].add(j)
            dic[j].add(i)
        visited = set()
        queue = collections.deque([dic.keys()[0]])
        while queue:
            node = queue.popleft()
            if node in visited:
                return False
            visited.add(node)
            for neighbour in dic[node]:
                queue.append(neighbour)
                dic[neighbour].remove(node)
            dic.pop(node)
        return not dic



        #DFS
        dic = {i:set() for i in range(n)}
        for i, j in edges:
            dic[i].add(j)
            dic[j].add(i)
        stack = [dic.keys()[0]]
        visited = set()
        while stack:
            node = stack.pop()
            if node in visited:
                return False
            visited.add(node)
            for neighbour in dic[node]:
                stack.append(neighbour)
                dic[neighbour].remove(node)
            dic.pop(node)
        return not dic
        