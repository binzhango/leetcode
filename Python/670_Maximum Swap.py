"""
Given a non-negative integer, you could swap two digits at most once to get the maximum valued number. 
Return the maximum valued number you could get.

Example 1:

Input: 2736
Output: 7236
Explanation: Swap the number 2 and the number 7.

Example 2:

Input: 9973
Output: 9973
Explanation: No swap.


9 8 3 6 8
    ----- find maxRight from i
    i

"""

class Solution:
    def maximumSwap(self, num):
        """
        :type num: int
        :rtype: int
        """
        num = str(num)
        i = 0
        #find the first increasing index
        while i<len(num)-1:
            if num[i]>=num[i+1]:
                i+=1
            else:
                break
        
        #find the larget one at the right part
        maxRight = num[i]
        maxIdx = i
        
        while i<len(num):
            if num[i]>=maxRight: # should add '=' becasue we need the right most largest one eg. 1 9 9 1
                maxRight = num[i]
                maxId =i
            i+=1
        
        #find the first one digit which is samller than the max digit
        j=0       
        while j<len(num):
            if num[j] >=maxRight:
                j+=1
            else:
                break
        # swap the digits
        num = list(num)
        if j<len(num):
            num[j], num[maxId] = num[maxId], num[j]
        
        # return result
        return int(''.join(num))