"""
or H-Index: What if the citations array is sorted in ascending order? Could you optimize your algorithm?
"""

# binary search 
class Solution(object):
    def hIndex(self, citations):
        """
        :type citations: List[int]
        :rtype: int
        """
        n = len(citations)
        l, h = 0, n-1
        while l<=h:
            mid = (l+h)/2
            if citations[mid]<n-mid:
                l = mid +1
            else:
                h = mid-1
        return n-l

            
        