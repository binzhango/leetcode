"""

Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).

For example, this binary tree [1,2,2,3,4,4,3] is symmetric:

    1
   / \
  2   2
 / \ / \
3  4 4  3

But the following [1,2,2,null,3,null,3] is not:

    1
   / \
  2   2
   \   \
   3    3
"""

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


# Iterative
class Solution(object):
    def isSymmetric(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        def isSym(left, right):
            if left and right and left.val==right.val:
                return isSym(left.left,right.right) and isSym(left.right, right.left)
            return left == right
        return isSym(root,root)


# Recursive

class Solution(object):
    def isSymmetric(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        if not root:
            return True
        stack = [[root.left, root.right]]
        
        while stack:
            node1, node2 = stack.pop()
            if node1 and node2:  # make sure not None
                if node1.val != node2.val:
                    return False
                else:
                    stack.append([node1.left, node2.right])
                    stack.append([node1.right, node2.left])
            else:
                if node1 == node2:
                    continue
                else:
                    return False
        return True