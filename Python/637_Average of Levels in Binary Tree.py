"""
Given a non-empty binary tree, return the average value of the nodes 
on each level in the form of an array.

Input:
    3
   / \
  9  20
    /  \
   15   7
Output: [3, 14.5, 11]
Explanation:
The average value of nodes on level 0 is 3,  on level 1 is 14.5, and 
on level 2 is 11. Hence return [3, 14.5, 11].
"""
# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


### BFS (deque)
class Solution(object):
    def averageOfLevels(self, root):
        """
        :type root: TreeNode
        :rtype: List[float]
        """
        q = collections.deque([root])
        result = []
        while q:
            val, n = 0, len(q)
            for i in xrange(n):
                node = q.popleft()
                val += node.val
                if node.left:
                    q.append(node.left)
                if node.right:
                    q.append(node.right)
            result.append(val/float(n))
        return result

### DFS 