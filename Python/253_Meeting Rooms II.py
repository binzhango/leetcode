"""
Given an array of meeting time intervals consisting of start and end times 
[[s1,e1],[s2,e2],...] (si < ei), find the minimum number of conference rooms required.

For example,
Given [[0, 30],[5, 10],[15, 20]],
return 2.

"""

# Definition for an interval.
# class Interval(object):
#     def __init__(self, s=0, e=0):
#         self.start = s
#         self.end = e

class Solution(object):
    def minMeetingRooms(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: int
        """
        intervals.sort(key=lambda x: x.start)
        heap=[]
        for i in intervals:
            if heap and i.start>=heap[0]:
                heapq.heapreplace(heap,i.end)
            else:
                heapq.heappush(heap,i.end)
        return len(heap)



    def minMeetingRooms(self, intervals):
        start = []
        end = []

        for i int intervals:
            start.append(i.start)
            end.append(i.end)
        
        start.sort()
        end.sort()


        result =0
        e =0

        for s in range(len(start)):
            if(start[s]<end[e]):
                result +=1
            else:
                e+=1
        return result