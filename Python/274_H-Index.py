"""
Given an array of citations (each citation is a non-negative integer) of a researcher, 
write a function to compute the researcher's h-index.
According to the definition of h-index on Wikipedia: "A scientist has index h if h of 
his/her N papers have at least h citations each, and the other N − h papers have no more 
than h citations each."
For example, given citations = [3, 0, 6, 1, 5], which means the researcher has 5 papers 
in total and each of them had received 3, 0, 6, 1, 5 citations respectively. 
Since the researcher has 3 papers with at least 3 citations each and the remaining two 
with no more than 3 citations each, his h-index is 3.
Note: If there are several possible values for h, the maximum one is taken as the h-index.
"""

class Solution(object):
    def hIndex(self, citations):
        """
        :type citations: List[int]
        :rtype: int
        """
        #O(NlogN)
        citations.sort()
        n = len(citations)
        for i in xrange(n): #change range() to xrange() because xrange() is faster then range() in 2.x
            if citations[i] >= (n-i):
                return n-i
        return 0

        #O(n) and space O(n)
        
        #the result can only range from 0 to the length of the array (because we can't have h-index 
        #greater than the total papers published). So we create an array "arr" which acts like a HashMap 
        # (using pigeon hole principle) and loop backwards from the highest element, then we find "tot" 
        # which is the total number of papers that has more than i citations, and we stop when 
        # tot>=i (total number of papers with more than i citations >= i). We don't need to keep going 
        # because we are trying the biggest i possible, we we stop and return the result.

        n = len(citations)
        citationCount = [0]*(n+1)
        for c in citations:
            if c >= n:
                citationCount[n] +=1
            else:
                citationCount[c] +=1
        i=n
        tot=0
        while i>=0:
            tot += citationCount[i]
            if tot>=i:
                return i
            i-=1
        return 0