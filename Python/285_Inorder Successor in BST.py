"""
Given a binary search tree and a node in it, find the in-order successor of that node in the BST.
"""

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def inorderSuccessor(self, root, p):
        """
        :type root: TreeNode
        :type p: TreeNode
        :rtype: TreeNode
        """
        succ = None
        while root:
            if p.val < root.val:
                succ = root
                root = root.left
            else:
                root = root.right
        return succ

    def inorderPredecessor(self, root,p):

        prode = None
        while root:
            if p.val <= root.val:
                root= root.left
            else:
                prode = root
                root =root.right
        return prode