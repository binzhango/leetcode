"""
Given an array nums, write a function to move all 0's to the end of it while maintaining 
the relative order of the non-zero elements.

For example, given nums = [0, 1, 0, 3, 12], after calling your function, 
nums should be [1, 3, 12, 0, 0].
"""


"""
use zero the keep the leftmost zero position

if found the non-zero value then swith the values and move the zero to next

a b 0 0 c

1: a!=0
    nums[0], nums[zero] = nums[zero], nums[0] zero=0
    zero =1
    b!=0
    nums[1], nums[1] = nums[1], nums[1] zero =2
    0
    skip
    0
    skip
    c!=0
    nums[4],nums[2] = nums[2], nums[4] zero =3


"""

class Solution(object):
    def moveZeroes(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        zero = 0; # zero is the index which points to the 0 and we need to fill it by some non-zero value
        for i in xrange(len(nums)):
            if nums[i]!=0:
                nums[i], nums[zero] = nums[zero], nums[i]
                zero+=1

                