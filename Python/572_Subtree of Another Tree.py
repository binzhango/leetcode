"""
Given two non-empty binary trees s and t, check whether tree t has exactly the same 
structure and node values with a subtree of s. A subtree of s is a tree consists of 
a node in s and all of this node's descendants. The tree s could also be considered 
as a subtree of itself.

Example 1:
Given tree s:

     3
    / \
   4   5
  / \
 1   2
Given tree t:
   4 
  / \
 1   2
Return true, because t has the same structure and node values with a subtree of s.
Example 2:
Given tree s:

     3
    / \
   4   5
  / \
 1   2
    /
   0
Given tree t:
   4
  / \
 1   2
Return false.


"""

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None



class Solution(object):


    """
    Serialize and Search O(M+N)

    Serialize s and t using their preorder traversal.
    Be careful to include left and right child of leaves.
    Corner case: [12], [2]. This will resolve to "12,#,#" and "2,#,#". The latter is a substring of the former but [2] is not a subtree of [12].
    Disambiguate by serializing s and t as: "(12)(#)(#)" and "(2)(#)(#)".
    Then test whether serialization of t is a substring of s.
    Substring match using KMP is O(M+N). Space used is O(M+N).
    """
    def isSubtree(self, s, t):
        """
        :type s: TreeNode
        :type t: TreeNode
        :rtype: bool
        """
        ss, ts = [],[]
        self.serialize(s, ss)
        self.serialize(t, ts)
        return "".join(ts) in "".join(ss)
        
    def serialize(self,root, arr): ## convert the tree to a list
        if not root:
            arr.append("#")
        else:
            arr.append("\(%s\)"%(root.val))
            self.serialize(root.left, arr)
            self.serialize(root.right, arr)
        return 

    ##################################################
    """
    Preorder Traversal O(MN)

    We need to test every node in s as a potential candidate for the root of a subtree which is the same as t.
    Traverse the tree in pre-order.
    For each node in the traversal, test if the node can be the root.
    Time complexity: O(MN). Space O(M+N)

    """

    def isSubtree(self, s, t):
    """
    :type s: TreeNode
    :type t: TreeNode
    :rtype: bool
    """
    if self.isMatch(s,t):
        return True
    elif not s:
        return False
    else:
        return self.isSubtree(s.left, t) or self.isSubtree(s.right, t)
    
    def isMatch(self, s, t):
        if not s and not t:
            return True
        elif s and t:
            return (s.val==t.val) and self.isMatch(s.left, t.left) and self.isMatch(s.right, t.right)
        else:
            return False